# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# ImpEx for SOLR Configuration

# Macros / Replacement Parameter definitions
$classificationCatalog=ShopClassification
$contentCatalog=shopContentCatalog
$classSystemVersion=systemVersion(catalog(id[default='ShopClassification']),version[default='1.0'])
$classCatalogVersion=catalogVersion(catalog(id[default='ShopClassification']),version[default='1.0'])
$classAttribute=classificationAttribute(code,$classSystemVersion)
$classClass=classificationClass(code,$classCatalogVersion)
$classAttributeAssignment=classAttributeAssignment($classClass,$classAttribute,$classSystemVersion)
$contentCatalogVersion=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalogName]),CatalogVersion.version[default=Staged])[default=$contentCatalogName:Staged]
$productCatalog=shopProductCatalog
$productCatalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged]


# Define Range Set
INSERT_UPDATE SolrValueRangeSet; name[unique=true]; qualifier; type  ; solrValueRanges(&rangeValueRefID)
                               ; WeightRange      ;          ; double; rangeWeight1,rangeWeight2,rangeWeight3,rangeWeight4,rangeWeight5,rangeWeight6,rangeWeight7,rangeWeight8
# Define Ranges
INSERT_UPDATE SolrValueRange; &rangeValueRefID; solrValueRangeSet(name)[unique=true]; name[unique=true]; from     ; to
                            ; rangeWeight1    ; WeightRange                         ; 0 - 1            ; 0        ; 0.9999
                            ; rangeWeight2    ; WeightRange                         ; 1 - 2            ; 1.0        ; 1.9999
                            ; rangeWeight3    ; WeightRange                         ; 2 - 3            ; 2.0        ; 2.9999
                            ; rangeWeight4    ; WeightRange                         ; 3 - 4            ; 3.0        ; 3.9999
                            ; rangeWeight5    ; WeightRange                         ; 4 - 5            ; 4.0        ; 4.9999
                            ; rangeWeight6    ; WeightRange                         ; 5 - 10           ; 5.0        ; 9.9999
                            ; rangeWeight7    ; WeightRange                         ; 10 - 15          ; 10.0       ; 14.9999
                            ; rangeWeight8    ; WeightRange                         ; 20 - 50          ; 20.0       ; 50

INSERT_UPDATE SolrIndexedProperty; solrIndexedType(identifier)[unique=true]; name[unique=true]       ; type(code); sortableType(code); currency[default=false]; localized[default=false]; multiValue[default=false]; facet[default=false]; facetType(code); facetSort(code); priority; visible; fieldValueProvider                         ; customFacetSortProvider     ; rangeSets(name); $classAttributeAssignment
                                 ; shopProductType                         ; Weight, 7001            ; double    ;                   ;                        ;                         ;                          ; true                ; MultiSelectOr  ; Custom         ; 1000    ; true   ; commerceClassificationPropertyValueProvider; numericFacetSortProviderDesc; WeightRange    ; 7000:::Weight, 7001::::
                                 ; shopProductType                         ; Display resolution, 3001; string    ;                   ;                        ;                         ;                          ; true                ; Refine         ; Alpha          ; 1000    ; true   ; commerceClassificationPropertyValueProvider;                             ;                ; 3000:::Display resolution, 3001::::

# Import Classification Features
INSERT_UPDATE SolrIndexedProperty; solrIndexedType(identifier)[unique=true]; name[unique=true]; type(code); localized[default=true]; multiValue[default=false]; fieldValueProvider                         ; $classAttributeAssignment
                                 ; shopProductType                         ; feature-hdtype   ; text      ; true                   ; false                    ; commerceClassificationPropertyValueProvider; 3000:::HD type, 3004::::

# Show the Classification Features in the Product List View Category /Search Pages
INSERT_UPDATE ClassAttributeAssignment; $classClass[unique=true]; $classAttribute[unique=true]; $classSystemVersion[unique=true]; listable[default=true]
                                      ; 3000                    ; HD type, 3004               ;                                 ; true

# Redirect page URL
INSERT_UPDATE SolrURIRedirect;url[unique=true]

INSERT_UPDATE SolrPageRedirect;redirectItem(uid,$contentCatalogVersion)[unique=true]

INSERT_UPDATE SolrCategoryRedirect;redirectItem(code,$productCatalogVersion)[unique=true]

INSERT_UPDATE SolrProductRedirect;redirectItem(code,$productCatalogVersion)[unique=true]

