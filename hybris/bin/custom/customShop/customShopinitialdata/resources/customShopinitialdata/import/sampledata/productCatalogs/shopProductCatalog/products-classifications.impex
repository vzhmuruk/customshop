$productCatalog = shopProductCatalog
$productCatalogName = ShopProductCatalog
$classificationCatalog = ShopClassification
$catalogVersion = catalogversion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$clAttrModifiers = system = 'ShopClassification', version = '1.0', translator = de.hybris.platform.catalog.jalo.classification.impex.ClassificationAttributeTranslator, lang = en


$feature1 = @Weight, 7001[$clAttrModifiers];  # Weight
$feature2 = @Width, 7002[$clAttrModifiers];  # Width
$feature3 = @Depth, 7003[$clAttrModifiers];  # Depth
$feature4 = @Height, 7004[$clAttrModifiers];  # Height
$feature5 = @VESA mounting, 1002[$clAttrModifiers]; # VESA mounting
$feature6 = @VESA mounting interfaces, 1003[$clAttrModifiers]; # VESA mounting interfaces
INSERT_UPDATE Product; code[unique = true]; $feature1; $feature2; $feature3; $feature4; $feature5   ;  $catalogVersion;
                     ; 0101               ; 3.0      ; 400      ; <ignore> ; 1500     ; <ignore>    ;
                     ; 0102               ; 2.0      ; 450      ; <ignore> ; 1600     ; <ignore>    ;
                     ; 0103               ; 1.0      ; 350      ; <ignore> ; 1450     ; <ignore>    ;
                     ; 0104               ; 2.0      ; 500      ; <ignore> ; 1600     ; <ignore>    ;
                     ; 0201               ; 3.0      ; 50       ; 5        ; 200      ; false       ;
                     ; 0202               ; 4.0      ; 45       ; 7        ; 220      ; VESA 100x100;
                     ; 0203               ; 4.0      ; 55       ; 6        ; 240      ; false       ;
                     ; 0204               ; 2.0      ; 65       ; 5        ; 200      ; VESA 100x100;
                     ; 0205               ; 4.0      ; 55       ; 6        ; 220      ; false       ;
                     ; 0301               ; 0.2      ; 500      ; 20       ; 500      ; <ignore>    ;
                     ; 0302               ; 0.4      ; 520      ; 25       ; 505      ; <ignore>    ;
                     ; 0303               ; 0.25     ; 550      ; 15       ; 515      ; <ignore>    ;
                     ; 0304               ; 0.2      ; 500      ; 17       ; 520      ; <ignore>    ;
                     ; 0401               ; 10.0     ; 639      ; 82       ; 1102     ; VESA 200x200;
                     ; 0402               ; 48.0     ; 1113     ; 434      ; 1744     ; VESA 100x100;
                     ; 0403               ; 12.0     ; 965      ; 207      ; 599      ; false       ;
                     ; 0404               ; 45.0     ; 1674     ; 66       ; 959      ; VESA 150x150;
                     ; 0405               ; 10.0     ; 1102     ; 82       ; 639      ; false       ;
                     ; 0501               ; 4.0      ; 429      ; 309      ; 35       ; <ignore>    ;
                     ; 0502               ; 5.0      ; 416      ; 272      ; 34       ; <ignore>    ;
                     ; 0503               ; 11.0     ; 333      ; 229      ; 16       ; <ignore>    ;


$feature1 = @Color, 1001[$clAttrModifiers];  # Color
INSERT_UPDATE Product; code[unique = true]; $feature1        ; $catalogVersion;
                     ; 0101               ; Black            ;
                     ; 0102               ; Black            ;
                     ; 0103               ; Light Wood       ;
                     ; 0104               ; Grey             ;
                     ; 0201               ; Black            ;
                     ; 0202               ; Black            ;
                     ; 0203               ; Black            ;
                     ; 0204               ; "White&amp;Black";
                     ; 0205               ; Black            ;
                     ; 0301               ; Black            ;
                     ; 0302               ; Black            ;
                     ; 0303               ; Black            ;
                     ; 0304               ; Black            ;
                     ; 0401               ; Black            ;
                     ; 0402               ; Black            ;
                     ; 0403               ; Black            ;
                     ; 0404               ; Black            ;
                     ; 0405               ; Black            ;
                     ; 0501               ; Black            ;
                     ; 0502               ; Black            ;
                     ; 0503               ; Black            ;

# Classification: Display (3003)
$feature1 = @Response time, 3002[$clAttrModifiers];  # Response time
$feature2 = @Display resolution, 3001[$clAttrModifiers];  # Display resolution (MP)
$feature3 = @Native aspect ratio, 3003[$clAttrModifiers];  # Native aspect ratio
$feature4 = @HD type, 3004[$clAttrModifiers];  # HD type
INSERT_UPDATE Product; code[unique = true]; $feature1; $feature2; $feature3; $feature4  ; $catalogVersion;
                     ; 0201               ; 6        ; 1920x1080; 24       ; Full HD    ;
                     ; 0202               ; 6        ; 1920x1080; 24       ; Full HD    ;
                     ; 0203               ; 6        ; 1920x1080; 27       ; Full HD    ;
                     ; 0204               ; 4        ; 1920x1080; 22       ; Full HD    ;
                     ; 0205               ; 4        ; 1920x1080; 22       ; Full HD    ;
                     ; 0301               ; <ignore> ; 1920x1080; 5.5      ; <ignore>   ;
                     ; 0302               ; <ignore> ; 1920x1080; 5        ; <ignore>   ;
                     ; 0303               ; <ignore> ; 1280x1024; 5.7      ; <ignore>   ;
                     ; 0304               ; <ignore> ; 2048x1536; 8        ; <ignore>   ;
                     ; 0401               ; <ignore> ; 3840x2160; 42       ; Ultra HD 4K;
                     ; 0402               ; <ignore> ; 3840x2160; 78       ; Ultra HD 4K;
                     ; 0403               ; <ignore> ; 3840x2160; 68       ; Ultra HD 4K;
                     ; 0404               ; <ignore> ; 3840x2160; 75       ; Ultra HD 4K;
                     ; 0405               ; <ignore> ; 3840x2160; 49       ; Ultra HD 4K;
                     ; 0501               ; <ignore> ; 3840x2160; 17       ; Ultra HD 4K;
                     ; 0502               ; <ignore> ; 1920x1080; 17       ; Full HD    ;
                     ; 0503               ; <ignore> ; 1920x1080; 14       ; Full HD    ;

# Classification: Audio (3003)
$feature1 = @BuiltIn speaker, 2001[$clAttrModifiers];  # Built-in speaker
$feature2 = @Number of speakers, 2002[$clAttrModifiers];  # Number of speakers
$feature3 = @Microphone in, 5004[$clAttrModifiers];  # Microphone in
$feature4 = @Combo headphone/mic port, 5005[$clAttrModifiers];  # Combo headphone/mic port
$feature5 = @RMS rated power, 2003[$clAttrModifiers];  # RMS rated power
INSERT_UPDATE Product; code[unique = true]; $feature1; $feature2; $feature3; $feature4; $feature5; $catalogVersion;
                     ; 0201               ; true     ; 2        ; false    ; <ignore> ; 20       ;
                     ; 0202               ; true     ; 2        ; false    ; <ignore> ; 15       ;
                     ; 0203               ; true     ; 2        ; false    ; <ignore> ; 10       ;
                     ; 0204               ; true     ; 2        ; false    ; <ignore> ; 25       ;
                     ; 0205               ; true     ; 4        ; false    ; <ignore> ; 10       ;
                     ; 0301               ; true     ; 2        ; false    ; <ignore> ; 15       ;
                     ; 0302               ; true     ; 2        ; false    ; <ignore> ; 20       ;
                     ; 0303               ; true     ; 1        ; false    ; <ignore> ; 5        ;
                     ; 0304               ; true     ; 4        ; false    ; <ignore> ; 50       ;
                     ; 0401               ; true     ; 4        ; false    ; <ignore> ; 60       ;
                     ; 0402               ; true     ; 1        ; true     ; <ignore> ; 25       ;
                     ; 0403               ; true     ; 2        ; false    ; <ignore> ; 40       ;
                     ; 0404               ; true     ; 1        ; true     ; <ignore> ; 20       ;
                     ; 0405               ; true     ; 2        ; false    ; <ignore> ; 75       ;
                     ; 0501               ; true     ; 4        ; true     ; true     ; 30       ;
                     ; 0502               ; true     ; 2        ; true     ; true     ; 40       ;
                     ; 0503               ; true     ; 4        ; true     ; true     ; 20       ;
