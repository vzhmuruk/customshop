package org.custom.shop.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.apache.log4j.Logger;
import org.custom.shop.core.service.impl.ComparePageServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

/**
 * Created on 13.09.17.
 */
@Controller
public class AddToCompareController extends AbstractPageController {

    @Resource(name = "comparePageService")
    private ComparePageServiceImpl comparePageService;
    private Logger LOG = Logger.getLogger(AddToCompareController.class);

    @RequestMapping(value = "/compare/add", method = RequestMethod.GET)
    public ResponseEntity<String> addToCompare(@RequestParam("productCode") final String code, final Model model) throws CMSItemNotFoundException {
        LOG.info("Product with code " + code + " has been successfully added");
        comparePageService.addProduct(code);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/comparePage")
    public String getAllProductsForCompare(final Model model) throws CMSItemNotFoundException {
        model.addAttribute("codesList", comparePageService.getAllProductsCodesForCompare());
        final ContentPageModel comparePage = getContentPageForLabelOrId("comparePage");
        storeCmsPageInModel(model, comparePage);
        setUpMetaDataForContentPage(model, comparePage);
        return getViewForPage(model);
    }

    @RequestMapping(value = "/compare/delete", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteProductFromCompare(@RequestParam("productCode") final String code, final Model model){

        boolean result = comparePageService.deleteProductFromCompare(code);
        if(result){
            model.addAttribute("result", comparePageService.deleteProductFromCompare(code));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/compare/clearpage")
    public ResponseEntity<String> deleteAllFromComparePage(){
        comparePageService.deleteAllFromComparePage();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
