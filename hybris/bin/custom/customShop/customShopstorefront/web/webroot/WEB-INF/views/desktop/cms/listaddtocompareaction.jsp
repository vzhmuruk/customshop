<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<div class="add_to_compare_button">

    <form class="addToCompareForm" method="get">
        <input type="hidden" id="productCode" name="productCode" class="productCode" value="${product.code}"/>
        <button class="addToCompare" type="submit">Compare</button>
    </form>
</div>

