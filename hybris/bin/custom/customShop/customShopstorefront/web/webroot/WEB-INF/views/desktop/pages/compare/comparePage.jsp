<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>


<template:page pageTitle="${comparePage}">
    <div id="globalMessages">
        <common:globalMessages/>
    </div>
    <cms:pageSlot position="Section1" var="comp" element="div">
        <cms:component component="${comp}"/>
    </cms:pageSlot>

    <c:set var="count" value="0"/>
    <div class="container">
        <c:forEach var="product" items="${codesList}">
            <div class="category">${product.key.toUpperCase()}</div>
            <br>
            <c:choose>
                <c:when test="${product.value.size() gt 1}">
                    <c:forEach items="${product.value}" var="product">
                        <div class="product">
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <c:if test="${not empty product.classifications}">
                                <tr>
                                    <td><a href="/customShopstorefront/shop/en/${product.url}">${product.name}&nbsp;${product.summary}&nbsp;${product.code}</a><br>
                                        <img src="${product.images.get(2).url}"/>
                                    </td>
                                    <td>
                                        <form class="deleteFromCompareForm" method="delete">
                                            <input type="hidden" id="productCode" name="productCode" class="productCode"
                                                   value="${product.code}"/>
                                            <input type="hidden" name="CSRFToken" value="${CSRFToken}"/>
                                            <button class="deleteFromCompare" type="submit">X</button>
                                        </form>
                                    </td>
                                </tr>
                                <c:forEach items="${product.classifications}" var="classification">
                                    <table>
                                        <td>
                                            <div class="headline">
                                                <c:choose>
                                                    <c:when test="${count lt 2}"><b>${classification.name}</b></c:when>
                                                    <c:otherwise><br></c:otherwise>
                                                </c:choose>
                                            </div>
                                        </td>

                                        <c:forEach items="${classification.features}" var="feature">
                                            <tr>
                                                <td
                                                        <c:if test="${count gt 1}">hidden="hidden"</c:if>
                                                >${feature.name}
                                                </td>
                                                <td>
                                                    <c:forEach items="${feature.featureValues}" var="value"
                                                               varStatus="status">
                                                        <c:if test="${value.value == false}">&nbsp;</c:if>
                                                        ${value.value}
                                                        <c:choose>
                                                            <c:when test="${feature.range}">
                                                                ${not status.last ? '-' : feature.featureUnit.symbol}
                                                            </c:when>
                                                            <c:otherwise>
                                                                ${feature.featureUnit.symbol}
                                                                ${not status.last ? '<br/>' : ''}
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </td>
                                            </tr>

                                        </c:forEach>
                                    </table>
                                </c:forEach>
                            </c:if>
                            </table>
                        </div>
                    </c:forEach>
                    <%--</table>--%>
                </c:when>
                <c:otherwise>
                    <h1 class="warning"><c:out value="Choose one or more products from the same category"/></h1>
                </c:otherwise>

            </c:choose>

        </c:forEach>
    </div>
    <c:if test="${empty codesList}"><h1 class="warning"><c:out
            value="Your compare products list is empty. Choose products..."/></h1></c:if>
    <form class="clearComparePage" method="delete">
        <input type="hidden" name="CSRFToken" value="${CSRFToken}"/>
        <button class="clearCompare" type="submit" <c:if test="${empty codesList}">disabled</c:if>> Clear Page</button>
    </form>
    </div>

    <style type="text/css">

        .container > div {
            vertical-align: top;
            display: inline-block;

        }

        .product {
            display: inline-block;
            margin: 0 auto;
        }

        .warning {
            font-size: 20px;
            color: #C7C7C8;
            text-align: center;
            padding: 100px;
        }

        .category {
            font-size: 35px;
            color: #3e6790;
            width: 100%;
            text-align: center;
            border-bottom: 1px solid #000;
            margin: 10px 0 20px;
        }

        .clearCompare {
            color: #C7C7C8;
            text-align: center;
            width: 100%;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</template:page>
