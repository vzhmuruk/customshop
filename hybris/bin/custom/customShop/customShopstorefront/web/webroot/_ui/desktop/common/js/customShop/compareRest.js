
    $(document).ready(function () {
        $(".deleteFromCompareForm").on("click", function (event) {
            event.preventDefault();
            var productCode = this.elements["productCode"].defaultValue;
            var url = "/customShopstorefront/shop/en/compare/delete?productCode=" + productCode;
            $.ajax({
                type: "delete",
                url: url,
                success: function () {
                    alert("Product " + productCode + " has been deleted");
                    location.reload();
                },
                error: function () {
                    alert("ERROR: "  + this.error.arguments[2])
                }
            });
            return true;
        });

        $(".addToCompareForm").on("click", function (event) {
            event.preventDefault();
            var productCode = this.elements["productCode"].defaultValue;
            var url = "/customShopstorefront/shop/en/compare/add?productCode=" + productCode;
            $.ajax({
                type: "get",
                url: url,
                // data: "productCode=" + productCode,
                success: function () {
                    alert("Product "+ productCode + " has been added");
                    location.reload();
                },
                error: function () {
                    alert("ERROR: "  + this.error.arguments[2])
                }
            });
            return true;
        });
        
        $(".clearComparePage").on("click", function (event) {
            event.preventDefault();
            var url = "/customShopstorefront/shop/en/compare/clearpage";
            $.ajax({
                type: "get",
                url: url,
                success: function () {
                    alert("All items have been removed!");
                    location.reload();
                },
                error: function () {
                    alert("ERROR: "  + this.error.arguments[2])
                }
            });
            return true;
        });
    });
