package org.custom.shop.core.service.impl;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.cronjob.model.ProductExecuterCronJobModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;
import org.custom.shop.core.dao.ProductExecutorJobDao;
import org.custom.shop.core.model.CronJobReportSenderProcessModel;
import org.custom.shop.core.service.ProductExecutorJobService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 28.07.17.
 */
public class ProductExecutorJobServiceImpl implements ProductExecutorJobService {

    private ProductExecutorJobDao productExecutorJobDao;

    private BusinessProcessService businessProcessService;
    private ModelService modelService;
    private SessionService sessionService;
    private MediaService mediaService;
    private EmailService emailService;
    private FlexibleSearchService flexibleSearchService;
    private CatalogVersionService catalogVersionService;
    private static final String COMMA_DELIMETER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String FILE_HEADER = "PK, Manufacturer, Model, Code, Sold amount";
    private static final String FILE_PATH = Config.getParameter("email.attachment.file.path");
    private static final String FILE_EXTENSION_CSV = Config.getParameter("email.attachment.file.extension");
    private static final String CATALOG_ID = "shopContentCatalog";
    private static final String CATALOG_VERSION_NAME = "Staged";
    private static final String MIME_TYPE = Config.getParameter("email.attachment.file.mime");
    private String FILE_NAME = Config.getParameter("email.attachment.file.name");

    private static final Logger LOG = Logger.getLogger(ProductExecutorJobServiceImpl.class);

    public void createCSVFile(List<ArrayList<String>> prodList) {
        FileWriter fileWriter = null;
        String fileAttributes = FILE_PATH + FILE_NAME + FILE_EXTENSION_CSV;
        try {
            fileWriter = new FileWriter(fileAttributes);
            LOG.info("Creating file..." + fileAttributes);
            fileWriter.append(FILE_HEADER + NEW_LINE_SEPARATOR);
            for (int i = 0; i < prodList.size(); i++) {
                for (int j = 0; j < prodList.get(i).size(); j++) {
                    fileWriter.append(prodList.get(i).get(j));
                    fileWriter.append(COMMA_DELIMETER);
                }
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
            LOG.info("File " + FILE_NAME + " has been created successfully in " + FILE_PATH);
        } catch (IOException e) {
            LOG.error("Error creating report file", e);
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException e) {
                LOG.error("Error while flushing/closing fileWriter", e);
            }
        }
    }

    public void sendEmailNotification(ProductExecuterCronJobModel prodExecutorJobModel) throws FileNotFoundException {
        CronJobReportSenderProcessModel jobReportSenderProcessModel = businessProcessService.createProcess(
                "cronJobReportSenderProcess" + System.currentTimeMillis(), "cronJobReportSenderProcess");

        EmailAttachmentModel attachment = processEmailAttachmentFile();

        jobReportSenderProcessModel.setEmailsList(prodExecutorJobModel.getEmailsList());
        jobReportSenderProcessModel.setAttachment(attachment);
        jobReportSenderProcessModel.setFrom(prodExecutorJobModel.getFrom());
        jobReportSenderProcessModel.setTo(prodExecutorJobModel.getTo());
        modelService.save(jobReportSenderProcessModel);
        prodExecutorJobModel.setAttachment(attachment);
        modelService.save(prodExecutorJobModel);
        businessProcessService.startProcess(jobReportSenderProcessModel);
    }

    private EmailAttachmentModel processEmailAttachmentFile() throws FileNotFoundException {
        final CatalogVersionModel contentCatalogVersion = catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION_NAME);
        FileInputStream fileInputStream = new FileInputStream(FILE_PATH + FILE_NAME + FILE_EXTENSION_CSV);

        final EmailAttachmentModel mediaFile = new EmailAttachmentModel();
        mediaFile.setCatalogVersion(contentCatalogVersion);
        mediaFile.setCode(FILE_NAME + Math.random());
        mediaFile.setMime(MIME_TYPE);
        mediaFile.setRealFileName(FILE_NAME + FILE_EXTENSION_CSV);
        modelService.save(mediaFile);

        final MediaFolderModel folder = new MediaFolderModel();
        folder.setQualifier(FILE_PATH + mediaFile.getPk());
        folder.setPath(FILE_PATH);
        modelService.save(folder);
        mediaService.setStreamForMedia(mediaFile, fileInputStream, FILE_NAME, MIME_TYPE, folder);
        return mediaFile;
    }

    public List<ArrayList<String>> findAllProductsSoldThanSpecifiedDays(Date from, Date to){
        return productExecutorJobDao.findAllProductsSoldThanSpecifiedDays(from, to);
    }

    public void setProductExecutorJobDao(ProductExecutorJobDao productExecutorJobDao) {
        this.productExecutorJobDao = productExecutorJobDao;
    }

    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
