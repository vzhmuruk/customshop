package org.custom.shop.core.service;

import de.hybris.platform.cronjob.model.ProductExecuterCronJobModel;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 28.07.17.
 */
public interface ProductExecutorJobService {
    List<ArrayList<String>> findAllProductsSoldThanSpecifiedDays(Date from, Date to);
    void createCSVFile(List<ArrayList<String>> prodList);
    void sendEmailNotification(ProductExecuterCronJobModel prodExecutorJobModel) throws FileNotFoundException;

}
