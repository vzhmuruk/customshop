package org.custom.shop.core.service;

import de.hybris.platform.commercefacades.product.data.ProductData;

/**
 * Created on 18.09.17.
 */
public interface ComparePageService {

    void addProduct(String productCode);
    ProductData getProductByCode(String code);
    boolean deleteProductFromCompare(String code);

}
