package org.custom.shop.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import org.custom.shop.core.constants.CustomShopCoreConstants;

@SuppressWarnings("PMD")
public class CustomShopCoreManager extends GeneratedCustomShopCoreManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( CustomShopCoreManager.class.getName() );
	
	public static final CustomShopCoreManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CustomShopCoreManager) em.getExtension(CustomShopCoreConstants.EXTENSIONNAME);
	}
	
}
