package org.custom.shop.core.actions;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import org.custom.shop.core.model.CronJobReportSenderProcessModel;

/**
 * Created on 31.08.17.
 */
public class RemoveEmailAttachmentAction extends AbstractProceduralAction<CronJobReportSenderProcessModel> {

    @Override
    public void executeAction(CronJobReportSenderProcessModel cronJobReportSenderProcessModel) throws Exception {
        getModelService().remove(cronJobReportSenderProcessModel.getAttachment());
    }
}
