package org.custom.shop.core.dao.impl;


import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.custom.shop.core.dao.ProductExecutorJobDao;

import java.util.*;

/**
 * Created on 28.07.17.
 */
public class ProductExecutorJobDaoImpl implements ProductExecutorJobDao {

    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<ArrayList<String>> findAllProductsSoldThanSpecifiedDays(Date from, Date to) {

        final String query = "SELECT {pr.PK},{pr.name},{pr.manufactureraid},{pr.code},count(*) FROM " +
                "{OrderEntry as oe JOIN Product AS pr ON {oe.product}={pr.pk} JOIN Order AS or ON {oe.order}={or.pk}}" +
                " WHERE {or.date} BETWEEN ?from AND ?to group by {pr.PK},{pr.manufacturername},{pr.name},{pr.code}";

        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("from", from);
        searchQuery.addQueryParameter("to", to);
        searchQuery.setResultClassList(Arrays.asList(String.class, String.class, String.class, String.class, String.class));
        SearchResult<ArrayList<String>> searchResult = flexibleSearchService.search(searchQuery);

        return searchResult.getResult();
    }

    private FlexibleSearchService getFlexibleSearchService() {
        return flexibleSearchService;
    }

    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }
}
