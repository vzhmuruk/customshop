package org.custom.shop.core.actions;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.email.EmailGenerationService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.strategies.ProcessContextResolutionStrategy;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import org.custom.shop.core.model.CronJobReportSenderProcessModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 17.08.17.
 */
public class CronJobSenderEmailAction extends AbstractSimpleDecisionAction<CronJobReportSenderProcessModel> {

    private CMSEmailPageService cmsEmailPageService;
    private String frontendTemplateName;
    private ProcessContextResolutionStrategy contextResolutionStrategy;
    private EmailGenerationService emailGenerationService;
    private CatalogVersionService catalogVersionService;


    @Override
    public Transition executeAction(CronJobReportSenderProcessModel businessProcessModel) throws Exception {
        final CatalogVersionModel contentCatalogVersion = catalogVersionService.getCatalogVersion("shopContentCatalog", "Staged");

        EmailPageModel emailPageModel = cmsEmailPageService.getEmailPageForFrontendTemplate(frontendTemplateName, contentCatalogVersion);
        if(emailPageModel != null) {
            EmailMessageModel emailMessageModel = emailGenerationService.generate(businessProcessModel, emailPageModel);
            List<EmailMessageModel> messagesList = new ArrayList<>();
            List<EmailAttachmentModel> attachmentList = new ArrayList<>();
            attachmentList.add(businessProcessModel.getAttachment());
            emailMessageModel.setToAddresses(businessProcessModel.getEmailsList());
            emailMessageModel.setAttachments(attachmentList);
            modelService.save(emailMessageModel);
            messagesList.add(emailMessageModel);
            businessProcessModel.setEmails(messagesList);
            modelService.save(businessProcessModel);
            return Transition.OK;
        }
        return Transition.NOK;
    }

    public void setContextResolutionStrategy(ProcessContextResolutionStrategy contextResolutionStrategy) {
        this.contextResolutionStrategy = contextResolutionStrategy;
    }

    public void setFrontendTemplateName(String frontendTemplateName) {
        this.frontendTemplateName = frontendTemplateName;
    }

    public void setCmsEmailPageService(CMSEmailPageService cmsEmailPageService) {
        this.cmsEmailPageService = cmsEmailPageService;
    }

    public void setEmailGenerationService(EmailGenerationService emailGenerationService) {
        this.emailGenerationService = emailGenerationService;
    }

    public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

}
