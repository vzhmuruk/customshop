package org.custom.shop.core.service.impl;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.log4j.Logger;
import org.custom.shop.core.model.ProductsComparatorModel;
import org.custom.shop.core.service.ComparePageService;

import java.util.*;

import static de.hybris.platform.core.Constants.TC.User;


/**
 * Created on 18.09.17.
 */
public class ComparePageServiceImpl implements ComparePageService {

    private ProductService productService;
    private ProductsComparatorModel productComparator;
    private ModelService modelService;
    private SessionService sessionService;
    private ProductFacade productFacade;
    private UserService userService;
    private static final Integer MAX_LIST_VALUE = 5;
    private static final String SESSION_COMPARE_PARAMETER_NAME = "compare";

    private Logger LOG = Logger.getLogger(ComparePageServiceImpl.class);


    @Override
    public void addProduct(String productCode) {
        UserModel currentUser = userService.getCurrentUser();

        if(!userService.isAnonymousUser(currentUser)){
            List<String> productsList;
            if(currentUser.getProductsComparator().size() == 0){
                productsList = new ArrayList<>();
                productsList.add(productCode);
                ProductsComparatorModel comparatorModel = modelService.create(ProductsComparatorModel.class);
                comparatorModel.setProductsList(productsList);
                modelService.save(comparatorModel);
                List<ProductsComparatorModel> prodComparatorModelList = new ArrayList<>();
                prodComparatorModelList.add(comparatorModel);
                currentUser.setProductsComparator(prodComparatorModelList);
                modelService.save(currentUser);
            } else {
                List<ProductsComparatorModel> productsComparator = currentUser.getProductsComparator();
                for(int i = 0; i < productsComparator.size(); i++){
                    ProductsComparatorModel productsComparatorModel = productsComparator.get(i);
                    List<String> productsCodesList = new ArrayList<>(productsComparatorModel.getProductsList());
                    if(!productsCodesList.contains(productCode))
                    {
                        productsCodesList.add(productCode);
                        productsComparatorModel.setProductsList(productsCodesList);
                        modelService.save(productsComparatorModel);
	                    currentUser.setProductsComparator(productsComparator);
                        modelService.save(currentUser);
                    }
                }
            }
        } else{
            if(hasSessionCart()){
                ProductsComparatorModel productsComparatorModel = getSessionComporator();
                List<String> productsList = productsComparatorModel.getProductsList();
                if(productsList == null){
                    productsList = new ArrayList<>();
                }
                List<String> productCodesList = new ArrayList<>(productsList);
                productCodesList.add(productCode);
                productsComparatorModel.setProductsList(productCodesList);
                modelService.save(productsComparatorModel);
                setSessionComporator(productsComparatorModel);
            } else {
                ProductsComparatorModel newComparatorModel = modelService.create(ProductsComparatorModel.class);
                List<String> productsCodesList = new ArrayList<>();
                productsCodesList.add(productCode);
                newComparatorModel.setProductsList(productsCodesList);
                modelService.save(newComparatorModel);
                setSessionComporator(newComparatorModel);
            }
        }
    }

    public Map<String, List<ProductData>> getAllProductsCodesForCompare(){
        List <String> productsListForCompare = new ArrayList<>(getUserProductsList());
        List<ProductData> products = new ArrayList<>();
        for (int i = 0; i < productsListForCompare.size(); i++)
        {
            products.add(getProductByCode(productsListForCompare.get(i)));
        }

        return sortProductsByCategory(products);
    }

    private List <String> getUserProductsList(){
        List <String> productsListForCompare;

        if(userService.isAnonymousUser(userService.getCurrentUser()))
        {
            productsListForCompare = getSessionComporator().getProductsList();
        } else {
            productsListForCompare = userService.getCurrentUser().getProductsComparator().get(0).getProductsList();
        }

        if(productsListForCompare == null){
            productsListForCompare = new ArrayList<>();
        }
        return productsListForCompare;
    }

    @Override
    public ProductData getProductByCode(String code) {
       return productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.CLASSIFICATION,
               ProductOption.BASIC, ProductOption.SUMMARY, ProductOption.CATEGORIES, ProductOption.PRICE,
               ProductOption.DESCRIPTION));
    }

    @Override
    public boolean deleteProductFromCompare(String code){


        for(int i = 0; i < productComparator.getProductsList().size(); i++){
            if(productComparator.getProductsList().get(i).equals(code)){
                productComparator.getProductsList().remove(i);
                return true;
            }
        }
        return false;
    }

    private Map<String, List<ProductData>> sortProductsByCategory(List<ProductData> products){
        Map<String, List<ProductData>> sortedProductsMap = new HashMap<>();
        for(ProductData product : products){
            Collection<CategoryData> categories = product.getCategories();
            CategoryData categoryData = categories.iterator().next();
            if(sortedProductsMap.isEmpty())
            {
                sortedProductsMap.put(categoryData.getName(), new ArrayList<>());
                sortedProductsMap.get(categoryData.getName()).add(product);
            } else {
                if(sortedProductsMap.containsKey(categoryData.getName())){
                    sortedProductsMap.get(categoryData.getName()).add(product);
                }
            }
        }

        return sortedProductsMap;
    }

    public boolean hasSessionCart()
    {
        try
        {
            return sessionService.getAttribute(SESSION_COMPARE_PARAMETER_NAME) != null;
        }
        catch (final JaloObjectNoLongerValidException ex)
        {
            if (LOG.isInfoEnabled())
            {
                LOG.info("Session Cart no longer valid. Removing from session. hasSessionCart will return false. " + ex.getMessage());
            }
            sessionService.removeAttribute(SESSION_COMPARE_PARAMETER_NAME);
            return false;
        }
    }

    public void setSessionComporator(ProductsComparatorModel comporator){
        if (comporator == null)
        {
            LOG.info("COMPARATOR IS NULL!");
        }
        else
        {
            sessionService.setAttribute(SESSION_COMPARE_PARAMETER_NAME, comporator);
        }
    }

    public ProductsComparatorModel getSessionComporator()
    {
        try
        {
            return internalGetSessionComparator();
        }
        catch (final JaloObjectNoLongerValidException ex)
        {
            if (LOG.isInfoEnabled())
            {
                LOG.info("Session Comporator no longer valid. Removing from session. getSessionComporator will create a new cart. "
                        + ex.getMessage());
            }
            sessionService.removeAttribute(SESSION_COMPARE_PARAMETER_NAME);
            return internalGetSessionComparator();
        }
    }

    protected ProductsComparatorModel internalGetSessionComparator()
    {
        final ProductsComparatorModel comparatorModel = sessionService.getOrLoadAttribute(SESSION_COMPARE_PARAMETER_NAME,
                new SessionService.SessionAttributeLoader<ProductsComparatorModel>()
                {
                    @Override
                    public ProductsComparatorModel load()
                    {
                        return modelService.create(ProductsComparatorModel.class);
                    }
                });
        return comparatorModel;
    }



    public void deleteAllFromComparePage(){
        if(userService.isAnonymousUser(userService.getCurrentUser())){
            modelService.remove(getSessionComporator());
        } else {
            modelService.remove(userService.getCurrentUser().getProductsComparator().get(0));
        }
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setProductComparator(ProductsComparatorModel productComparator) {
        this.productComparator = productComparator;
    }

    public ProductsComparatorModel getProductComparator() {
        return productComparator;
    }

    public void setProductFacade(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    public void setSessionService(SessionService sessionService)
    {
        this.sessionService = sessionService;
    }
}
