package org.custom.shop.core.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.ProductExecuterCronJobModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.log4j.Logger;
import org.custom.shop.core.service.ProductExecutorJobService;

import java.io.FileNotFoundException;

/**
 * Created on 27.07.17.
 */
public class ProductExecutorJob extends AbstractJobPerformable<ProductExecuterCronJobModel> {

    private BusinessProcessService businessProcessService;
    private ProductExecutorJobService productExecutorJobService;
    private ModelService modelService;
    private SessionService sessionService;
    private FlexibleSearchService flexibleSearchService;

    private static final Logger LOG = Logger.getLogger(ProductExecutorJob.class);


    @Override
    public PerformResult perform(ProductExecuterCronJobModel cronJobModel) {
        LOG.info("------------------- CronJob has been started -------------------");
        productExecutorJobService.createCSVFile(productExecutorJobService.findAllProductsSoldThanSpecifiedDays(cronJobModel.getFrom(), cronJobModel.getTo()));
        try {
            productExecutorJobService.sendEmailNotification(cronJobModel);
        } catch (FileNotFoundException e) {
            LOG.error("CronJob has been crashed! " + e.getMessage());
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }
        LOG.info("------------------- CronJob has been finished -------------------");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    public void setProductExecutorJobService(ProductExecutorJobService productExecutorJobService) {
        this.productExecutorJobService = productExecutorJobService;
    }

    public BusinessProcessService getBusinessProcessService() {
        return businessProcessService;
    }

    public void setBusinessProcessService(BusinessProcessService businessProcessService) {
        this.businessProcessService = businessProcessService;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

    @Override
    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    @Override
    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    @Override
    public void setFlexibleSearchService(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}
