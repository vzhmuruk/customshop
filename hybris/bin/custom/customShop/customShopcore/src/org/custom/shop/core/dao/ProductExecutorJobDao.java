package org.custom.shop.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created on 28.07.17.
 */
public interface ProductExecutorJobDao {

    List<ArrayList<String>> findAllProductsSoldThanSpecifiedDays(Date from, Date to);
}
