package org.custom.shop.facades.reviewVideo;

import org.custom.shop.core.model.ReviewVideoModel;
import org.custom.shop.facades.converters.ProductVideoConverter;
import org.custom.shop.facades.product.data.ReviewVideoData;
import org.custom.shop.facades.reviewVideo.impl.ReviewVideoFacade;


/**
 * Created on 10.10.17.
 */
public class ReviewVideoFacadeImpl implements ReviewVideoFacade
{

	private ProductVideoConverter productVideoConverter;


	@Override
	public ReviewVideoData getReviewVideo(ReviewVideoModel model)
	{
		return productVideoConverter.convert(model);
	}


	public void setProductVideoConverter(ProductVideoConverter productVideoConverter)
	{
		this.productVideoConverter = productVideoConverter;
	}
}
