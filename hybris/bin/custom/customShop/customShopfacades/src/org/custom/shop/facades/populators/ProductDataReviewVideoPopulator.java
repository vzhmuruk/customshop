package org.custom.shop.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.custom.shop.core.model.ReviewVideoModel;
import org.custom.shop.facades.product.data.ReviewVideoData;
import org.custom.shop.facades.reviewVideo.impl.ReviewVideoFacade;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 12.10.17.
 */
public class ProductDataReviewVideoPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		implements Populator<SOURCE, TARGET>

{
	private ReviewVideoFacade reviewVideoFacade;

	@Override public void populate(SOURCE source, TARGET target) throws ConversionException
	{
		List<ReviewVideoData> videoDataList = new ArrayList<>();

		List<ReviewVideoModel> videoModelsList = source.getVideo();
		for(ReviewVideoModel videoModel : videoModelsList){
			videoDataList.add(reviewVideoFacade.getReviewVideo(videoModel));
		}

		target.setReviewVideoData(videoDataList);
	}

	public void setReviewVideoFacade(ReviewVideoFacade reviewVideoFacade)
	{
		this.reviewVideoFacade = reviewVideoFacade;
	}
}
