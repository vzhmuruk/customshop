package org.custom.shop.facades.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.custom.shop.core.model.ReviewVideoModel;
import org.custom.shop.facades.product.data.ReviewVideoData;


/**
 * Created on 12.10.17.
 */
public class ProductVideoConverter implements Converter<ReviewVideoModel, ReviewVideoData>
{
	@Override public ReviewVideoData convert(ReviewVideoModel reviewVideoModel) throws ConversionException
	{
		ReviewVideoData reviewVideoData = new ReviewVideoData();
		return convert(reviewVideoModel, reviewVideoData);
	}

	@Override public ReviewVideoData convert(ReviewVideoModel reviewVideoModel, ReviewVideoData reviewVideoData)
			throws ConversionException
	{
		reviewVideoData.setId(reviewVideoModel.getId());
		reviewVideoData.setUrl(reviewVideoModel.getUrl());
		reviewVideoData.setProductName(reviewVideoModel.getProductName());
		return reviewVideoData;
	}
}
