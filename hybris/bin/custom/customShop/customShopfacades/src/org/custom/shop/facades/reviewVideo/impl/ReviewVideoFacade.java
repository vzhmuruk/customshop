package org.custom.shop.facades.reviewVideo.impl;

import org.custom.shop.core.model.ReviewVideoModel;
import org.custom.shop.facades.product.data.ReviewVideoData;


/**
 * Created on 10.10.17.
 */
public interface ReviewVideoFacade
{
 	ReviewVideoData getReviewVideo(ReviewVideoModel model);
}
