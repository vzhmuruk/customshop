package org.custom.shop.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.custom.shop.core.model.ReviewVideoModel;
import org.custom.shop.facades.product.data.ReviewVideoData;


/**
 * Created on 09.10.17.
 */
public class ProductVideoPopulator implements Populator<ReviewVideoModel, ReviewVideoData>
{

	@Override
	public void populate(ReviewVideoModel source, ReviewVideoData target) throws ConversionException
	{
		target.setUrl(source.getUrl());
		target.setProductName(source.getProductName());
		target.setId(source.getId());
	}
}
