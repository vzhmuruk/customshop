package org.custom.shop.facades.process.email.context;


import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.custom.shop.core.model.CronJobReportSenderProcessModel;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created on 15.08.17.
 */
public class CronJobReportSenderEmailContext extends AbstractEmailContext<CronJobReportSenderProcessModel> {

    private String DATE_FROM = "from";
    private String DATE_TO = "to";

    @Override
    public void init(final CronJobReportSenderProcessModel cronJobReportSenderProcessModel, final EmailPageModel emailPageModel) {

        put(FROM_EMAIL, "customshopemail@customshop.com");
        put(FROM_DISPLAY_NAME, "Customer Service");
        put(EMAIL, "vzhmuruk@gmail.com");
        put(DISPLAY_NAME, "Hello!");
        put(DATE_FROM, formatDate(cronJobReportSenderProcessModel.getFrom()));
        put(DATE_TO, formatDate(cronJobReportSenderProcessModel.getTo()));

    }

    private String formatDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        return sdf.format(date);
    }

    @Override
    protected BaseSiteModel getSite(final CronJobReportSenderProcessModel cronJobReportSenderProcessModel) {
        return null;
    }

    @Override
    protected CustomerModel getCustomer(final CronJobReportSenderProcessModel cronJobReportSenderProcessModel) {
        return null;
    }

    @Override
    protected LanguageModel getEmailLanguage(CronJobReportSenderProcessModel businessProcessModel) {
        return null;
    }
}
